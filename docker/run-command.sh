#!/bin/bash

set -eu

# E.g. `composer test`
COMMAND="$1"

# Set the workdir to the project root.
cd "${0%/*}/.."

docker run -v "$(pwd)":/app/ asset-uploader sh -c "cd /app/ && $COMMAND"
