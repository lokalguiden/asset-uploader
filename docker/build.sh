#!/bin/bash

set -eu

# Set the workdir to the project root.
cd "${0%/*}/.."

DOCKER_VERSION=$(docker -v | sed -e 's/^Docker version //' | awk -F'.' '{print $1}')
if [ "$DOCKER_VERSION" -lt 20 ]; then
    echo "You need docker 20 or later to compile this image"
    exit 1
fi

docker build -t asset-uploader -f docker/Dockerfile .

