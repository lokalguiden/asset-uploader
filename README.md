# Asset Uploader

Uploads assets to an AWS S3 bucket.

## Publishing a New Version

GitLab notifies [packagist](https://packagist.org/packages/lokalguiden/asset-uploader)
automatically when pushing tags or merging pull requests. To publish a new version:

1. Update the version in `composer.json` *(use semver)*.
2. Merge the merge request.
3. Create a tag and push it.

## License

Proprietary.
