<?php declare(strict_types=1);

namespace Tests\Lokalguiden\AssetUploader\FileMagic;

use Lokalguiden\AssetUploader\FileMagic\MimeTypeReader;
use PHPUnit\Framework\TestCase;
use RuntimeException;

class MimeTypeReaderTest extends TestCase
{
    public function testMimeTypeForZeroByteFile(): void
    {
        $this->assertEquals('inode/x-empty', MimeTypeReader::fromBuffer(''));
    }

    public function testFigureOutFromPng(): void
    {
        $buffer = $this->readFile('png.png');
        $this->assertEquals('image/png', MimeTypeReader::fromBuffer($buffer));
    }

    public function testFigureOutFromJpeg(): void
    {
        $buffer = $this->readFile('jpeg.jpg');
        $this->assertEquals('image/jpeg', MimeTypeReader::fromBuffer($buffer));
    }

    public function testFigureOutFromWebp(): void
    {
        $buffer = $this->readFile('webp.webp');
        $this->assertEquals('image/webp', MimeTypeReader::fromBuffer($buffer));
    }

    public function testDefaultToOctetStreamMimeType(): void
    {
        $buffer = chr(0x00).chr(0x01).chr(0x02).chr(0x03).chr(0x04).chr(0x05);

        $this->assertEquals('application/octet-stream', MimeTypeReader::fromBuffer($buffer));
    }

    public function testFigureOutFromGif(): void
    {
        $buffer = $this->readFile('gif.gif');
        $this->assertEquals('image/gif', MimeTypeReader::fromBuffer($buffer));
    }

    public function testTextAboutGifDoesNotIdentifyAsGif(): void
    {
        $this->assertNotEquals('image/gif', MimeTypeReader::fromBuffer('Let me tell you about GIF87a'));
        $this->assertNotEquals('image/gif', MimeTypeReader::fromBuffer('What\'s the deal with GIF89a?'));
    }

    public function testFigureOutFromFakeSvg(): void
    {
        $buffer = $this->readFile('svg.svg');
        $this->assertEquals('image/svg', MimeTypeReader::fromBuffer($buffer));
    }

    public function testFigureOutFromSvgWithXmlDeclaration(): void
    {
        $buffer = $this->readFile('svgxml.svg');
        $this->assertEquals('image/svg+xml', MimeTypeReader::fromBuffer($buffer));
    }

    public function testFigureOutFromPdf(): void
    {
        $buffer = $this->readFile('pdf.pdf');
        $this->assertEquals('application/pdf', MimeTypeReader::fromBuffer($buffer));
    }

    private function readFile(string $filename): string
    {
        $fullFilename = __DIR__ . '/' . $filename;
        $result = file_get_contents($fullFilename);

        if (false === $result) {
            throw new RuntimeException('Could not open file: ' . $fullFilename);
        }

        return $result;
    }
}
