<?php declare(strict_types=1);

namespace Lokalguiden\AssetUploader\FileMagic;

/**
 * Figures out mime type based on file content.
 *
 * This document has been helpful when making this: https://en.wikipedia.org/wiki/List_of_file_signatures
 */
class MimeTypeReader
{
    public const JPEG_MIME_TYPE = 'image/jpeg';

    /**
     * @param string $buffer A file read into a string.
     * @return string Mime type of content in $buffer, e.g. "image/png"
     */
    public static function fromBuffer(string $buffer): string
    {
        // Magic isn't stored beyond the first few bytes.
        $firstPartOfBuffer = substr($buffer, 0, 100);

        $bytes = strlen($buffer) > 0
            ? array_map(fn ($x) => ord($x), str_split($firstPartOfBuffer))
            : [];

        switch (true) {
            case 0 === count($bytes):
                return 'inode/x-empty';
            case self::isPng($bytes):
                return 'image/png';
            case self::isJpeg($bytes):
                return self::JPEG_MIME_TYPE;
            case self::isWebp($bytes):
                return 'image/webp';
            case self::isGif($firstPartOfBuffer):
                return 'image/gif';
            case self::isSvgPlusXml($firstPartOfBuffer):
                return 'image/svg+xml';
            case self::isSvg($firstPartOfBuffer):
                return 'image/svg';
            case self::isPdf($firstPartOfBuffer):
                return 'application/pdf';
            default:
                return 'application/octet-stream';
        }
    }

    /**
     * @param array<int> $bytes
     */
    private static function isPng(array $bytes): bool
    {
        if (count($bytes) < 8) return false;
        if (0x89 !== $bytes[0]) return false;
        if (0x50 !== $bytes[1]) return false;
        if (0x4e !== $bytes[2]) return false;
        if (0x47 !== $bytes[3]) return false;
        if (0x0d !== $bytes[4]) return false;
        if (0x0a !== $bytes[5]) return false;
        if (0x1a !== $bytes[6]) return false;
        if (0x0a !== $bytes[7]) return false;
        return true;
    }

    private static function isGif(string $bytes): bool
    {
        $check = substr($bytes, 0, 6);

        if ('GIF87a' === $check) return true;
        if ('GIF89a' === $check) return true;
        return false;
    }

    /**
     * WEBP contains the file's size in its magic bytes.
     * To keep things short, we do not check for this.
     *
     * @param array<int> $bytes
     */
    private static function isWebp(array $bytes): bool
    {
        if (count($bytes) < 12) return false;
        if (0x52 !== $bytes[0]) return false;
        if (0x49 !== $bytes[1]) return false;
        if (0x46 !== $bytes[2]) return false;
        if (0x46 !== $bytes[3]) return false;
        // Skip checking bytes 4-7 due to them being variable.
        if (0x57 !== $bytes[8]) return false;
        if (0x45 !== $bytes[9]) return false;
        if (0x42 !== $bytes[10]) return false;
        if (0x50 !== $bytes[11]) return false;
        return true;
    }

    /**
     * @param array<int> $bytes
     */
    private static function isJpeg(array $bytes): bool
    {
        if (self::isJpegVariant1($bytes)) return true;
        if (self::isJpegVariant2($bytes)) return true;
        if (self::isJpegVariant3($bytes)) return true;
        if (self::isJpegVariant4($bytes)) return true;
        return false;
    }

    /**
     * @param array<int> $bytes
     */
    private static function isJpegVariant1(array $bytes): bool
    {
        if (count($bytes) < 4) return false;
        if (0xff !== $bytes[0]) return false;
        if (0xd8 !== $bytes[1]) return false;
        if (0xff !== $bytes[2]) return false;
        if (0xdb !== $bytes[3]) return false;
        return true;
    }

    /**
     * @param array<int> $bytes
     */
    private static function isJpegVariant2(array $bytes): bool
    {
        if (count($bytes) < 4) return false;
        if (0xff !== $bytes[0]) return false;
        if (0xd8 !== $bytes[1]) return false;
        if (0xff !== $bytes[2]) return false;
        if (0xee !== $bytes[3]) return false;
        return true;
    }

    /**
     * @param array<int> $bytes
     */
    private static function isJpegVariant3(array $bytes): bool
    {
        if (count($bytes) < 12) return false;
        if (0xff !== $bytes[0]) return false;
        if (0xd8 !== $bytes[1]) return false;
        if (0xff !== $bytes[2]) return false;
        if (0xe0 !== $bytes[3]) return false;
        if (0x00 !== $bytes[4]) return false;
        if (0x10 !== $bytes[5]) return false;
        if (0x4a !== $bytes[6]) return false;
        if (0x46 !== $bytes[7]) return false;
        if (0x49 !== $bytes[8]) return false;
        if (0x46 !== $bytes[9]) return false;
        if (0x00 !== $bytes[10]) return false;
        if (0x01 !== $bytes[11]) return false;
        return true;
    }

    /**
     * @param array<int> $bytes
     */
    private static function isJpegVariant4(array $bytes): bool
    {
        if (count($bytes) < 12) return false;
        if (0xff !== $bytes[0]) return false;
        if (0xd8 !== $bytes[1]) return false;
        if (0xff !== $bytes[2]) return false;
        if (0xe1 !== $bytes[3]) return false;
        // Skip two bytes due to them not being part of the magic.
        if (0x45 !== $bytes[6]) return false;
        if (0x78 !== $bytes[7]) return false;
        if (0x69 !== $bytes[8]) return false;
        if (0x66 !== $bytes[9]) return false;
        if (0x00 !== $bytes[10]) return false;
        if (0x00 !== $bytes[11]) return false;
        return true;
    }

    private static function isSvg(string $buffer): bool
    {
        return (false !== mb_strpos($buffer, 'http://www.w3.org/2000/svg'));
    }

    private static function isSvgPlusXml(string $buffer): bool
    {
        if (false === mb_strpos($buffer, '<?xml')) {
            return false;
        }

        if (false === mb_strpos($buffer, 'http://www.w3.org/2000/svg')) {
            return false;
        }

        return true;
    }

    private static function isPdf(string $bytes): bool
    {
        $check = substr($bytes, 0, 5);

        if ('%PDF-' === $check) return true;
        return false;
    }
}
