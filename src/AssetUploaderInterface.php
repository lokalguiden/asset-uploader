<?php declare(strict_types=1);

namespace Lokalguiden\AssetUploader;

interface AssetUploaderInterface
{
    /**
     * @param string $destination Relative path to desired storage location. May contain directories to categorize
     *                            the asset. Example: `uploads/dishes/beef-burger-john.jpg`
     * @param string $binaryAssetData Contents of the file you want to upload.
     * @return string URL where one can reach the uploaded asset.
     * @throws UploadAssetException On upload failure.
     */
    public function upload(string $destination, string $binaryAssetData): string;
}
