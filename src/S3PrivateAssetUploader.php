<?php declare(strict_types=1);

namespace Lokalguiden\AssetUploader;

use Aws\S3\S3ClientInterface;
use Lokalguiden\AssetUploader\AbstractS3AssetUploader;

/**
 * You'll need one instance of this class per bucket you want to upload private assets to.
 */
class S3PrivateAssetUploader extends AbstractS3AssetUploader
{
    public function __construct(
        S3ClientInterface $s3client,
        string $bucket
    ) {
        parent::__construct($s3client, $bucket, 'private');
    }
}
