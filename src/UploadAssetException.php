<?php declare(strict_types=1);

namespace Lokalguiden\AssetUploader;

use RuntimeException;

class UploadAssetException extends RuntimeException
{
}
