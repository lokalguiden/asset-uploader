<?php declare(strict_types=1);

namespace Lokalguiden\AssetUploader;

use Aws\S3\S3Client;
use Aws\S3\S3ClientInterface;

/**
 * Create an S3 client based on configuration values.
 * Passing the `use_path_style_endpoint` setting as a boolean wasn't possible
 * in `services.xml`, resulting in this workaround.
 */
class S3ClientFactory
{
    public static function createS3Client(
        string $endpoint,
        string $region,
        bool $usePathStyleEndpoint,
        string $accessKey,
        string $secretKey
    ): S3ClientInterface {
        return new S3Client(
            [
                'version' => '2006-03-01',
                'endpoint' => $endpoint,
                'region' => $region,
                'credentials' => [
                    'key' => $accessKey,
                    'secret' => $secretKey,
                ],
                'use_path_style_endpoint' => $usePathStyleEndpoint,
            ]
        );
    }
}
