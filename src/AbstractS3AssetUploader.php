<?php declare(strict_types=1);

namespace Lokalguiden\AssetUploader;

use Aws\Result;
use Aws\S3\S3ClientInterface;
use Aws\S3\ObjectUploader;
use Lokalguiden\AssetUploader\FileMagic\MimeTypeReader;
use Lokalguiden\AssetUploader\JpegOrientationFixer\JpegOrientationFixer;

abstract class AbstractS3AssetUploader implements AssetUploaderInterface
{
    public function __construct(
        private S3ClientInterface $s3client,
        private string $bucket,
        private string $acl
    ) {
    }

    public function upload(
        string $destination,
        string $binaryAssetData
    ): string {
        $mimeType = MimeTypeReader::fromBuffer($binaryAssetData);

        $uploader = new ObjectUploader(
            $this->s3client,
            $this->bucket,
            $destination,
            $binaryAssetData,
            $this->acl,
            [
                'params' => [
                    'ContentType' => $mimeType,
                    'CacheControl' => 'max-age=31536000',
                ],
            ]
        );

        /** @var Result<string|array> $result */
        $result = $uploader->upload();

        if (200 !== $result['@metadata']['statusCode']) {
            throw new UploadAssetException(
                'Unexpected status code from S3-compatible service: '.$result['@metadata']['statusCode']
            );
        }

        if (!isset($result['ObjectURL']) || !is_string($result['ObjectURL'])) {
            throw new UploadAssetException('S3-compatible service reported unexpected object URL after upload.');
        }

        return $result['ObjectURL'];
    }
}
