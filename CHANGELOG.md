# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [2.4.0] - 2022-03-24

* Remove support for rotating JPEG files

## [2.3.1] - 2021-08-27

* Fixes bug where some jpegs aren't classified as jpeg

## [2.3.0] - 2021-08-23

* Removes PHP 7.4 support
* Fixes jpeg orientations if they have a EXIF orientation value

## [2.2.1] - 2021-04-22

* Updates some dependencies as dictated by `composer update`
* Adds PDF support

## [2.2.0] - 2021-04-21

Fixes PHP8 compatibility issues by updating `aws/aws-sdk-php` and refactoring `Lokalguiden\AssetUploader\FileMagic\MimeTypeReader`.

## [2.0.0] - 2020-06-01

### BREAKING CHANGES

Splits the `S3BucketAssetUploader` class into a public and private version.
